# Capes
Capes mod for Minecraft 1.14.4 using fabric.
For the Minetest version see the Minetest branch.

# How to use?
1. `./gradlew build`
2. Copy the Capes-<version>.jar file to your .minecraft/mods folder.
3. Open the capes button in your inventory.
4. Select a cape, or none.

# Available capes
Cape textures are found in assets/capes.
You can also use your vanilla Minecraft or OptiFine cape.
If you like you can also disable your cape.

# Servers
This mod is clientside and you do not need to install it on servers.
